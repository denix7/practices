/**
 * 
 * @author (Dennis Padilla) 
 * @version (v1.0)
 */
public class Challenge2 {
    public static String highLow(String s) {
        int max;
        int min;
        String res = "";
        String[] numbers = s.split(" ");

        max = Integer.parseInt(numbers[0]);
        min = Integer.parseInt(numbers[0]);
        
        for(String current : numbers)
        {
            int currentNumber = Integer.parseInt(current);   
            
            if(currentNumber > max)
                max = currentNumber;
                
            else if(currentNumber < min)
                min = currentNumber;
        }
        
        if(max == min)
            res = max + " " + max;
            
        else
            res = max + " " + min;
        
        return res;
    }
}