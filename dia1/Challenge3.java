/**
 * 
 * @author (Dennis Padilla) 
 * @version (v1.0)
 */

public class Challenge3 {
    public static int primorial(int n) 
    {
        int cantPrimes = 0;
        int multPrimes = 1;
        boolean allPrimesFound = false;
	    int i = 2;
	
        if(n == 1)
            return 2;
        
        while(!allPrimesFound && cantPrimes<n)
        {
            if(isPrime(i))
            {
                multPrimes = multPrimes * i;
                cantPrimes++;
            }
            
            i++;
        }
        
        return multPrimes;
    }
        
    public static boolean isPrime(int num)
    {
        int counter = 2;
        boolean isPrime = true;
        
        while(isPrime && (counter != num))
        {
            if(num % counter == 0)
                isPrime = false;
            
            counter++;
        }
        
        return isPrime;
    }
}