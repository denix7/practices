/**
 * 
 * @author (Dennis Padilla) 
 * @version (1.0)
 *  
 * El "algoritmo euclidiano"
 */
public class Challenge1 {
    public static int euclidean(int a, int b) {
        int r = 0;
        
        if(a < b)
        {
            swap(a, b);
        }
        
        r = a % b;
        if(r == 0)
            return b;
        else
        {
            return euclidean(b, r);
        }
    }
    
    public static void swap(int a,int b){
        int aux = a;
        a = b;
        b = a;
    }
}
