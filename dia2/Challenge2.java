/**
 * 
 * @author (Dennis Padilla) 
 * @version (1.0)
 *  
 */

public class Challenge {
  public static boolean magic(String str) {
		boolean res      = false;
        String pattern   = " ";
        String[] numbers = str.split(pattern);
        
        int day   = Integer.parseInt(numbers[0]);
        int month = Integer.parseInt(numbers[1]);
        int year  = Integer.parseInt(numbers[2]);
        
        int numberToFound = day * month;
        
        if(str.endsWith(Integer.toString(numberToFound)))
        {
            res = true;
        }
        
        return res;
  }
}