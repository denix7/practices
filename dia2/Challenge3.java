/**
 * 
 * @author (Dennis Padilla) 
 * @version (1.0)
 *  
 */

public class Program {
	public static double myPi(int num) {
		double res = 0.0;
	 	double pi  = Math.PI;
	 	
		String format    = "%." + num + "f";
	 	String formatedd = String.format(format, pi);

	 	res = Double.parseDouble(formatedd);
		
  	    return res;
	}
}