/**
 * 
 * @author (Dennis Padilla) 
 * @version (1.0)
 *  
 * ArrayOfMultiples
 */

import java.util.*;

public class Program {
	public static int[] arrayOfMultiples(int num, int length) {
		int[] res = new int[length];
        
        res[0] = num;
        for(int i=2; i<=length; i++)
        {
            res[i-1] = num*i;
        }

        return res;
	}
}